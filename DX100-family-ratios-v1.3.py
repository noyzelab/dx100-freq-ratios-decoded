# DX100 / 4 op FM family ratios generator v1.3
# Dave Burraston - Aug 2023 - www.noyzelab.com
#
# calculates the complete set of spectrally unique c:m families for DX100 et al
# where the ratios are always integers.
# works for any FM synth supporting integer ratios..
# writes the c:m ratios to a file called dx100farey.txt
#
# decoded using the Farey Sequence of order 15:
# [(0/1), (1/15), (1/14), (1/13), (1/12), (1/11), (1/10), (1/9), (1/8), (2/15),
# (1/7), (2/13), (1/6), (2/11), (1/5), (3/14), (2/9), (3/13), (1/4), (4/15),
#(3/11), (2/7), (3/10), (4/13), (1/3), (5/14), (4/11), (3/8), (5/13), (2/5),
# (5/12), (3/7), (4/9), (5/11), (6/13), (7/15), (1/2), (8/15), (7/13), (6/11),
# (5/9), (4/7), (7/12), (3/5), (8/13), (5/8), (7/11), (9/14), (2/3), (9/13),
# (7/10), (5/7), (8/11), (11/15), (3/4), (10/13), (7/9), (11/14), (4/5), (9/11),
# (5/6), (11/13), (6/7), (13/15), (7/8), (8/9), (9/10), (10/11), (11/12), (12/13),
# (13/14), (14/15), (1/1)]
# and extracting out the 'Normal Form' sequence.
# Also included are the 0/1 and 1/2 as suggested in Barry Truax's paper.

# ref for this Farey sequence method:
# Truax, B. 1978. Organizational Techniques for C:M Ratios in Frequency Modulation,
# Computer Music Journal, 1, 4. Note this paper is reprinted in Roads C. & Strawn J.
# eds. Foundations of Computer Music,  MIT Press, 1985.
# A number of key FM papers are reproduced in this book!

# carriers are the numerators
dx100farey_numerator = [0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1, 2, 1, 3, 2, 3, 1, 4, 3, 2,
                        3, 4, 1, 5, 4, 3, 5, 2, 5, 3, 4, 5, 6, 7, 1]

# modulators are the denominators
dx100farey_denominator = [1, 15, 14, 13, 12, 11, 10, 9, 8, 15, 7, 13, 6, 11, 5, 14, 9, 13,
                          4, 15, 11, 7, 10, 13, 3, 14, 11, 8, 13, 5, 12, 7, 9, 11, 13, 15, 2]

n=0 # var for current fam member
pos = 0 # positive fam member by addition - pos = c+n*m
neg = 0 # negative fam member by subtraction - neg = abs(c-n*m)

maxratio = 64 # used in the while loop to breakout once a ratio threshold is reached.
# the while loop code is a bit of a quick hack, and will be changed at some point! but its good enough for now.

totalratios = 0

# note that some families will head off into high ratios very quickly
# while others will have more ratios below 15, the max integer ratio on the DX100  

with open('dx100farey.txt','w') as f:
    msg = "DX100 / 4op spectrally unique integer families in total = " + str(len(dx100farey_numerator)) + "\n"
    print(msg)
    f.writelines(msg)

    # setup the farey fraction value for the cm ratio
    for num in range(0,len(dx100farey_numerator)):
        c = dx100farey_numerator[num]
        m = dx100farey_denominator[num]
        msg = "\n"
        f.writelines(msg)
        print(c, ":", m, "family")
        msg = ["\n", str(c), ":", str(m), " family" , "\n"]
        f.writelines(msg)
        print("member", ' \tneg',  ' \tpos')
        msg = ["member", " \tneg",  " \tpos" , "\n"]
        f.writelines(msg)
    # compute the pos & neg carriers for each nth family member 
        n = 1
        neg = abs(c-n*m) 
        pos = c+n*m
        print("n = ", n, ' \t', neg,  ' \t', pos)
        msg = ["n = ", str(n), ' \t', str(neg),  ' \t', str(pos), "\n"]
        f.writelines(msg)
        totalratios += 1 
        while (neg < maxratio) & (pos < maxratio):              
            n += 1
            neg = abs(c-n*m)
            pos = c+n*m
            if (neg > maxratio) & (pos > maxratio):
                break
            print("n = ", n, ' \t', neg,  ' \t', pos)
            msg = ["n = ", str(n), ' \t', str(neg),  ' \t', str(pos), "\n"]
            f.writelines(msg)
            totalratios += 1 
        print("famdecimal\t", round(1-round(c/m,2),2), "\t", round(c/m,2), "\n")
        msg = ["famdecimal\t", str(round(1-round(c/m,2),2)), "\t", str(round(c/m,2)), "\n"]
        f.writelines(msg)
        pos = 0 
        neg = 0 

    print("total ratios = ", 2*totalratios)
    msg = ["\ntotal ratios = ", str(2*totalratios)]
    f.writelines(msg)
f.close   

