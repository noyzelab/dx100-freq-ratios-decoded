# fm-ratio-tstr.py
# v1.2
# Python code to generate Addendum: √2, π/2 and √3 to 37x multiplication as part of the DX100 FREQUENCY RATIO CHART DECODED & ORGANISED repository
# u can use this code to generate new charts to higher resolution or larger multiplication factors for DX7 et al with larger FM ratios
# Dave Burraston Aug 2023
# www.noyzelab.com

r2 = 1.414 # root2
r2o2 = 0.71
r3 = 1.73 # root3
r3o2 = 0.87
po2 = 1.57 # pi/2
po4 = 0.87
sigfigs = 2
multiplier = 38

print("mul", "\t root2", "\t pi/2", "\t root3")
print(0.5, "\t", r2o2, "\t", po4, "\t", r3o2, "\t") 
for n in range(1,multiplier):
    print(n, "\t", round(r2*n, sigfigs),   "\t", round(po2*n, sigfigs), "\t", round(r3*n, sigfigs))
    
