**DX100 FREQUENCY RATIO CHART DECODED & ORGANISED**

![](https://gitlab.com/noyzelab/dx100-freq-ratios-decoded/-/raw/main/DX100-orig-working-crop.jpg)


The Yamaha DX100 owners manual contains a very interesting chart of its "carefully chosen" frequency ratios, although there is barely any discussion about them at all or what they actually are or relate to.. 

These mysterious ratios also appear in the other 4, 6 & 8 operator Yamaha FM synthesizers/chips, such as the DX7, DX21, TX81Z, FB-01, DX11, FS1R, DEXED, you name it.. so the information here is relevant for any FM synthesizer capable of producing these ratios. 

With a little deciphering using two tiny Python programs it turns out this mysterious chart contains: 3 groups of inharmonic ratios - √2, π/2, and √3, and a set of integer "multiplier" ratios [which also includes the simple ratio 0.5 ie 1/2] which can be decoded using the Farey Sequence resulting in 37 spectrally unique families. Included in this repo are:

- **Spreadsheet in Libre Office & PDF format with two sheets** deciphering and organising these ratios into a more coherent layout. Complete inharmonic ratio tables giving a highest ratio of 64.01, encompassing the complete range of the ASM Hydrasynth mutant ratios. Listing of the 37 spectrally unique C:M ratio families as organised by the Farey Sequence.
- **Python programs** to produce the tables. Edit & rerun this code to generate output for higher ratios.
- **Spectrally Unique Ratios in FM synthesis** - a short document covering the background behind using the Farey Sequence to organise C:M ratios. Includes an appendix with the complete listing of each spectrally unique family detailing every DX100 C:M ratio.

The original DX100 owners manual showing these ratios is available from here: https://usa.yamaha.com/products/contents/music_production/downloads/manuals/index.html?k=&c=music_production&l=en&p=5

Yamaha have been quite cunning, as you will see from the tables I've made decoding this mysterious chart. The inclusion of these 3 inharmonic ratio groups has been done in such a way that they contain exact multiples of themselves. This is incredibly useful when programming an FM sound using just sine waves, and as Yamaha hint they "produce extremely complex waveforms" for things like "sound effects including extremely realistic bells, explosions, etc." 

Below is a simple example I setup and tried on my DX7ii :

Consider creating a sound containing inharmonic timbres, where you would like to have a complex wave modulating a simple sine wave carrier tracking the keyboard normally. This could easily be achieved with a 3 operator sine stack 3>2>1. For the carrier [operator 1] you could choose a ratio of 1.0. Then you could setup operator 2 at √2 = 1.41 creating an inharmonic ratio, and modulate it with operator 3 using √2 but at x 2 = 2.82. This would give you a 2:1 ratio for operators 3 & 2 respectively and create a nice complex modulating wave with minimal to no beating. You could further adjust either or both operator 2 & 3 ratios using √2 as the inharmonic ratio to keep it whole number [integer] based.. or move across to another inharmonic set for one or both and explore futher from there.

if u find this repo useful please think about supporting my work either thru my bandcamp page, eg my PhD is available as pay what u want [or free download]
https://noyzelab.bandcamp.com/album/generative-music-cellular-automata-phd-thesis-data-2006

thanks, dave

**additional special thanks to Zoë Blade and Dougall Irving** for writing to me regarding this research. both wrote me regarding the re-arranging of this repository to √2, π/2, and √3. Zoë in Dec 2022 and Dougall in June 2021. V1.2 has now been reformatted and this results in a clearer layout overall.
